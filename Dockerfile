FROM ubuntu:18.04

RUN apt update -y
RUN apt upgrade -y
RUN apt install vim python3.7 python3-pip -y
RUN mkdir /worker
COPY ./src /worker
RUN mkdir /root/.aws

