
import logging
import asyncio
import time
import random
import gc
import tracemalloc

def prepare_logger(logger_name):
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)

    logger = logging.getLogger(logger_name)
    logger.addHandler(ch)
    logger.setLevel(logging.DEBUG)

    return logger

class AsyncExample():
    def __init__(self, logger=None):
        self.logger = logger
        self.num_meows = 0
        self.num_tasks = 0
        self.capacity = None

        self.bad_cats_yarn = None
        self.cool_cats_yarn = None

        self.storedsnapshot = None

    async def bad_cat(self):
        while True:
            async with self.bad_cats_yarn:
                self.logger.info('* badcat plays with his yarn *')
                await asyncio.sleep(2)
                self.logger.info('BADCAT: But can I get both yarns for a minute tho?')
                
                async with self.cool_cats_yarn:
                    await asyncio.sleep(2)
                    self.logger.info('* oh SNAP!  badcat plays with BOTH yarns!!  watta badass *')
                    self.logger.info('BADCAT: k you can have that back now...')

            # Take a break badcat!
            await asyncio.sleep(0.01)

    async def cool_cat(self):
        while True:
            async with self.cool_cats_yarn:
                self.logger.info('* coolcat plays with his yarn *')
                await asyncio.sleep(2)
                self.logger.info('COOLCAT: But can I get both yarns for a minute tho?')

                try:
                    await asyncio.wait_for(self.bad_cats_yarn.acquire(), 5)
                    self.bad_cats_yarn.release()
                except asyncio.TimeoutError:
                    self.logger.info('COOLCAT: ...you asshole')
                
                # async with self.bad_cats_yarn:
                #     await asyncio.sleep(2)
                #     self.logger.info('* oh SNAP!  coolcat plays with BOTH yarns!!  watta cool guy * ')
                #     self.logger.info('COOLCAT: k you can have that back now...')

            # Take a break coolcot!
            await asyncio.sleep(0.01)

    async def meow_task(self):
        self.num_tasks += 1

        # Pretend this is some I/O bound work, a write to mongo or dynamo for example
        await asyncio.sleep(random.random() * 10)
        self.num_meows += 1
        logger.info('A cat meows...asynchronously? Times meowed: {}.'.format(self.num_meows))

        self.capacity.release()
        self.num_tasks -= 1

    def death_by_catlock(self):
        loop = asyncio.get_event_loop()
        self.bad_cats_yarn = asyncio.Semaphore(1, loop=loop)
        self.cool_cats_yarn = asyncio.Semaphore(1, loop=loop)

        loop.run_until_complete(asyncio.gather(self.bad_cat(), self.cool_cat()))

    async def asyncio_main_loop(self):
        loop = asyncio.get_running_loop()
        self.capacity = asyncio.Semaphore(25, loop=loop)

        tracemalloc.start()

        while True:
            self.logger.info('Acquiring work...')

            # Pretend to get some work, imagine this is a call out to SQS
            await asyncio.sleep(random.random() / 10)
            await self.capacity.acquire()

            self.logger.info('Work acquired, creating task...')
            asyncio.create_task(self.meow_task())

            self.logger.info('Number of tasks in memory: {}'.format(self.num_tasks))

    # Some code that previous helped me debug memory leaks.  Might be useful later.
    def help_me(self):
        gc.collect()
        snapshot1 = tracemalloc.take_snapshot()
        
        filters = [tracemalloc.Filter(inclusive=False, filename_pattern="*tracemalloc*")]
        top_stats = None
        if self.storedsnapshot is not None:
            top_stats = snapshot1.filter_traces(filters).compare_to(self.storedsnapshot.filter_traces(filters), 'lineno')
            # top_stats = snapshot1.compare_to(self.storedsnapshot, 'lineno')
        self.storedsnapshot = snapshot1

        if top_stats is not None:
            self.logger.info('TRACER --- [ Top 10 differences ]')
            for stat in top_stats[:10]:
                self.logger.info('TRACER --- {}'.format(stat))


if __name__ == '__main__':
    print('Running...')
    logger = prepare_logger('aio_logger')
    logger.info('Logger initialized.')

    example = AsyncExample(logger)
    # asyncio.run(example.death_by_catlock())
    example.death_by_catlock()
